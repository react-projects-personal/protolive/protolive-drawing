import './polyfillImports';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Switch, HashRouter } from 'react-router-dom';
import { createHashHistory } from 'history';
import Login from 'pages/Login/Login';
// import ForgotPassword from 'pages/Login/ForgotPassword';
// import ForgotPasswordConfirm from 'pages/Login/ForgotPasswordConfirm';
// import Confirm from 'pages/Login/Verify';
// import Expired from 'pages/Exceptions/Exceptions';
// import TimeOut from 'pages/Exceptions/TimeOut';
import AppRoot from './App.js';
import PrivateRoute from 'services/checkAuth';
import * as serviceWorker from './serviceWorker';
import configureStore from './redux-state/store/configureStore'

//use react hooks to validate authentication
const history = createHashHistory();
const store = configureStore()

const Application = (props) => {
    return (
        <div>
            <Provider store={store.store}>
                <HashRouter>
                    <Switch>
                        <Route exact path="/" component={Login} />  
                        {/* <Route exact path="/timeout" component={TimeOut} />  
                        <Route exact path="/forgotpassword" component={ForgotPassword} />  
                        <Route exact path="/app/resetpassword" component={ForgotPasswordConfirm} />                  
                        <Route exact path="/app/confirm" component={Confirm} />
                        <Route exact path="/app/invite/expired" component={Expired} /> */}
                        <PrivateRoute component={AppRoot} />
                    </Switch>
                </HashRouter>
            </Provider>
        </div>        
    )
}

ReactDOM.render(<Application history={history}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
