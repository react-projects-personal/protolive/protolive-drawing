import React from 'react'
import { SketchField, Tools } from 'react-sketch';
import { Row, Col, Select } from 'antd'
const { Pencil, Circle } = Tools

class SketchFieldDemo extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedTool: Pencil
        }
    }


    setTool = (value) => {
        let selectedTool = Pencil;

        if (value === 'Pencil') {
            selectedTool = Pencil
        } else if (value === 'Circle') {
            selectedTool = Circle
        } else if (value === 'Selection') {
            selectedTool = Tools.Select
        }

        this.setState({ selectedTool })
    }
    render() {
        const Option = Select.Option;
        return (
            <React.Fragment>

                <Row type="flex" justify="center">
                    <Col span={24}>
                        <div style={{ background: 'lightGray', padding: '10px' }}>
                            <Row type="flex" justify="space-between">
                                <Col span={6}>
                                    <div style={{ background: 'rgba(0, 0, 0, 0.65)', padding: '10px', height: '400px' }}>
                                        <Select defaultValue="Pencil" style={{ width: '70%' }} onChange={this.setTool}>
                                            <Option value="Pencil">Pencil</Option>
                                            <Option value="Circle">Circle</Option>
                                            <Option value="Selection">Selection</Option>
                                        </Select>
                                    </div>
                                </Col>
                                <Col span={18}>
                                    <SketchField
                                        width='100%'
                                        height="400px"
                                        tool={this.state.selectedTool}
                                        lineColor='black'
                                        lineWidth={1}
                                        backgroundColor="white"
                                        heightCorrection={1}
                                    />
                                </Col>
                            </Row>

                        </div>
                    </Col>
                </Row>
            </React.Fragment >
        )
    }
}

export default SketchFieldDemo