import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { withRouter } from 'react-router';
import { connect } from "react-redux";
import menuActions from 'redux-state/modules/menu/actions';
import { Link } from 'react-router-dom';
import logo from 'assets/pov_dark.svg';
import logoText from 'assets/pov_dark_text.svg';
import './index.less';

const { Sider } = Layout;
const SubMenu = Menu.SubMenu;
class SideBar extends Component {
    _isMounted = false;

    state = {
        collapsed: false,
        abilities: (this.props.abilities)?this.props.abilities:[],
        selectedKey: this.props.location.pathname,
        openKey: [this.props.subMenus[this.props.location.pathname]],
        routes: (this.props.subMenus)?this.props.subMenus:{}
    };

    constructor(props){
        super(props);

        console.log(props)
        const locListener = this.props.history.listen((location, action) => {            
                            if(this.state.selectedKey !== location.pathname && this._isMounted){
                                this.setOpenKeys(this.props.subMenus[location.pathname], "change");
                                this.setHeaderKey(location);
                                this.setState({ selectedKey : location.pathname});            
                            }
                        });
        this.unlistener = locListener.unlisten;               
    }
    
    componentDidMount(){
        this._isMounted = true;
        this.setHeaderKey(this.props.location);
    }

    componentWillUnmount() {
        if(this.unlistener){
            this.unlistener();
        }        
        this._isMounted = false;
    }

    createMenu = () => {
       return (<Menu theme="dark" mode="inline" defaultSelectedKeys={['/app/home']} selectedKeys={[this.state.selectedKey]} openKeys={this.state.openKey}  onClick={this.setCollapsedForm}>
                    {
                        this.state.abilities.map((item,id) => this.createMenuComponent(item,id+1))
                    }  
                </Menu>);
    }

    createSubMenu = (item,index, route) => {        
        return (<Menu.Item key={route}>
                    <Link to={route}>
                        {item}
                    </Link>
                </Menu.Item>);
    }

    setOpenKeys = (key,option) => {        
        this.props.setHeader(key);
        const { openKey } = this.state;

        if (option === "click"){ //Click means the user is trying to collapse or uncollapse the submenu. So remove or add element accordingly
            if(this.state.collapsed){
                this.setState({ openKey: [ key ]})
            }else{
                if(openKey.indexOf(key) === -1){
                    openKey.push(key);
                    this.setState({ openKey });
                }else{
                    openKey.splice(openKey.indexOf(key),1);
                    this.setState({ openKey });
                }
            }            
        }else if(option === "change"){ //change means this triggered because of route change. In this case simply add the submenu to the array if it's not there already to collapse the submenu
            if(openKey.indexOf(key) === -1){
                openKey.push(key);
                this.setState({ openKey });
            }
        }
        
    }

    setHeaderKey = ({pathname}) => {
        
        if(pathname !== '/app/home'){           
            const sub = this.state.abilities.filter(ab => {                
                const filtered = ab.routes.filter(rt => rt === pathname);
                return filtered.length > 0
            })
            if(sub.length === 1) this.props.setHeader(sub[0].subject);                        
        }else if(pathname === '/app/home'){
            this.props.setHeader("Dashboard");
        }
    }

    setCollapsedForm = ({ item, key, keyPath, domEvent }) =>{
        if(this.state.collapsed){
            this.setState({ openKey: ['']})
        }  
        
        if(key !== '/app/home' && keyPath.length === 1){           
            const sub = this.state.abilities.filter(ab => {                
                return ab.routes[0] === key
            })            
            this.props.setHeader(sub[0].subject);
        }else if(key === '/app/home'){
            this.props.setHeader("Dashboard");
        }
    }

    createMenuComponent = (item, index) => {
        const idx = index;
        if(item.submenu){            
            return (<SubMenu
                        key={item.subject} onTitleClick={({key,domEvent}) => this.setOpenKeys(key, "click")}
                        title={<span><Icon type={item.icon} /><span className="nav-text">{item.subject}</span></span>}
                    >
                        {
                            item.menuslug.map((menuitem, id) => 
                                    this.createSubMenu(menuitem, `${idx}`, item.routes[id]))
                        }                  
                    </SubMenu>);
        }else{
            return (
                <Menu.Item key={item.routes[0]}>
                    <Link to={item.routes[0]}>
                        <Icon type={item.icon} />
                        <span className="nav-text">{item.subject}</span>
                    </Link>
                </Menu.Item>
            )
        }
    }    

    render() {

        return (
            <Sider
                style={{
                overflow: 'auto',
                position: 'fixed',
                left: 0
                }}
                width={256}
                theme="dark"
                breakpoint="lg"
                trigger={null}
                collapsible
                onCollapse={(collapsed) => { 
                    if(collapsed){
                        this.setState({ collapsed, openKey: [''] });                        
                    }else{
                        this.setState({ collapsed, openKey: [this.props.subMenus[this.props.location.pathname]] });
                    }
                    this.props.setCollapse(collapsed);
                }}
                className="sider"
            >
                <div className="logo" id="logo">
                    {/* < src={logo} alt="Logo" className="logo-img"/>                    
                    <img src={''} alt="Petco Allocation" className="text-img" />                                         */}
                    <h1><span style={{fontFamily: 'Montserrat', fontWeight:500, fontSize: '30px', color:'white'}}>Drawing</span> <span style={{fontFamily: 'Montserrat', fontWeight:300, fontSize: '30px'}}></span></h1>            
                </div>
                {this.createMenu()}                
            </Sider>
        );
  }
}

// export default Todo;
export default withRouter(connect(null,{ setCollapse: menuActions.setCollapse })(SideBar));