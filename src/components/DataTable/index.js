import React, {useState, useEffect} from 'react'
import { Table, Row, Col} from 'antd';
import './table.css'

export default function DataTable(props) {
  const { columns, data, pageSize, loading, prev, next, showPager, totalCount, defaultCurrent } = props;
  
  const [pageNo, setPage ] = useState(defaultCurrent);
  let pagingOptions = false;

  // console.log("Default current received =", defaultCurrent, pageNo);
  /**
   * Every row in a table should have a unique key as per react.
   * This will initialize all the objects in data array with an index key to
   * be used with rowKey attribute of <Table/>
   */
  useEffect(() => {           
    setPage(defaultCurrent);         
  }, [defaultCurrent]);

  const dataWithKeys = data.map((el,i) => {
    var o = Object.assign({}, el);
    o.key_id = `${i}-key`;
    return o;
  })

  function itemRender(current, type, originalElement) {
    if (type === 'prev') {
      return originalElement;
    }
    if (type === 'next') {
      return originalElement;
    }
  }

  if(showPager){
    pagingOptions = {
        pageSize: pageSize, 
        total: totalCount,
        showLessItems: true,
        itemRender: itemRender,
        current: defaultCurrent,
        onChange: (page, pageSize) => {
          
          if(page > pageNo){
            next();
          }else if (page <= pageNo){
            prev();
          }
        }
      };
  }
  
  return (
    <Row type="flex" justify="end">
      <Col span={24}>
        <Table 
            columns={columns} 
            loading={loading} 
            dataSource={dataWithKeys} 
            pagination={pagingOptions} 
            rowKey={(record) => record.key_id}            
            // scroll={{ y: 700 }}
            />
      </Col>
    </Row>
  )
}

/**
 * Some tables may not need the previous next buttons. In that case
 * showPager can be sent as false to the DataTable component <DataTable showPager={false} ... />
 */

DataTable.defaultProps = {
  showPager: true,
  totalCount: 0,
  pageSize: 0
};