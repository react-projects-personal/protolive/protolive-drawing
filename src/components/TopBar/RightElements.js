import React, { Component } from 'react';
import { Menu, Icon, Avatar, Dropdown, message } from 'antd';
import { withRouter } from "react-router";
import { signOut } from 'services/aws_services'
import './index.less';
import './dropDown.less'

class RightElements extends Component {
    _isMounted = false;

    state={
        email: '',
        name: '',
        isOrgid: '',
        initials: ''
    }

    handleClick = async (e) => {
        if(e.key === 'logout'){
            try{
                this.props.history.push(`/`);
            }catch(err){
                message.error('Unable to logout. Please try again later.');
            }
        }else if (e.key === 'account'){
            this.props.history.push('/app/account');
        }
    };

    setInitials = (name) => {
        if(!name) return;

        const initialArr = name.split(" ").map((n)=>n[0]);
        const init = (initialArr.length > 1)? `${initialArr[0]}${initialArr[initialArr.length - 1]}` : initialArr[0];
        const initials = init.toUpperCase();
        this.setState({ initials });
    }

    getDetails = async() => {

        const name = 'Vijay c'
        const isOrgid = true  
        const email = 'vijay.chavre@petco.com'     

        if(this._isMounted)
            this.setState({ email, name, isOrgid }, () => {
                this.setInitials((name)? name : email);
            });
    }


    componentDidMount(){
        this._isMounted = true;
        this.getDetails();
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    render() {
        const menu = (
            <Menu className="menu" selectedKeys={[]} onClick={this.handleClick}>
                <Menu.Item key="loggedin" style={{fontSize: '12px', cursor: "auto"}} disabled>
                    Logged in as <br/> {this.state.email}
                </Menu.Item>
                {
                    (this.state.isOrgid) &&
                    <Menu.Item key="account">
                        <Icon type="user" />
                        My account
                    </Menu.Item>
                }
                <Menu.Divider />
                <Menu.Item key="logout">
                    <Icon type="logout" />
                    Logout
                </Menu.Item>
            </Menu>
        );

        return (
        <div className="right">
            <Dropdown overlay={menu} >
                <span className="action account">
                    <Avatar style={{ backgroundColor: "#f56a00", verticalAlign: 'middle' }} size="small">
                        {this.state.initials}
                    </Avatar>
                    <span> {(this.state.name)? this.state.name : this.state.email}</span>
                </span>
            </Dropdown>
        </div>
        )
    }
}

export default withRouter(RightElements);