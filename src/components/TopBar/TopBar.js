import React, { Component } from 'react';
import { Layout, Typography } from 'antd';
import { connect } from 'react-redux';
import HeaderContext from 'contexts/HeaderContext';
import './index.less'
import RightElement from './RightElements';

const { Header } = Layout;
const { Title } = Typography;

class TopBar extends Component {
  static contextType = HeaderContext;

  render() {
    const { collapsed } = this.props;
    return (
        <Header style={{ padding: 0, zIndex: 2 }} className="fixedHeader">
          <div className="header">
            <div style={{marginLeft: (collapsed) ? 80 : 256}} className="left-item">   
              <Title level={3} style={{fontWeight: '300'}}>{this.context}</Title>
            </div>
            <RightElement />
          </div>          
        </Header>
    )
  }
}

function mapStateToProps(state) {
  return {
    collapsed: state.Menu.collapsed
  }
}

export default connect(mapStateToProps)(TopBar);