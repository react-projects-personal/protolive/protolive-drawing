import React from 'react';


/**
 * this function used validate the password against validation rule.
 * @param {*} rule 
 * @param {string} value 
 * @param {function} callback 
 */
export function strongValidator(rule,value,callback) {
    const reg = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})");
    const lower = new RegExp("^(?=.*[a-z])");
    const upper = new RegExp("^(?=.*[A-Z])");
    const numeric = new RegExp("^(?=.*[0-9])");
    const symbol = new RegExp("^(?=.[!@#$%^&])");
    const length = new RegExp("^(?=.{8,})");
    if(value && !reg.test(value)){
        callback(
        <div>
            <ul style={{textAlign: 'left', color: 'grey'}}>
                {
                    (!length.test(value))?
                    <li>Must be atleast 8 Characters</li>
                    :undefined
                }
                {
                    (!upper.test(value))?
                    <li>Must contain atleast 1 uppercase character</li>
                    :undefined
                }                    
                {
                    (!lower.test(value))?
                    <li>Must contain atleast 1 lowercase character</li>
                    :undefined
                }
                {
                    (!symbol.test(value) || !numeric.test(value))?
                    <li>Must contain atleast 1 number and 1 special character</li>
                    :undefined
                }
                
            </ul>
        </div>);
    }else{
        callback();
    }
};

/**
 * this function used comapre the confirmPassword with newPassword and validates if match equal.
 * @param {*} rule 
 * @param {string} value 
 * @param {Function} callback 
 */
export function compareToFirstPassword (rule, value, callback) {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("Password doesn't match!");
    } else {
      callback();
    }
};

/**
 * this function used validate nextPassword
 * @param {*} rule 
 * @param {string} value 
 * @param {function} callback 
 */
export function validateToNextPassword (rule, value, callback){
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    if(value && form.getFieldValue("confirm") && value !== form.getFieldValue("confirm")){
        form.validateFields(["confirm"], { force: false });
    }
    callback();
};
