

/**
 * this function returns formatted currency with respective locale and currency provided
 * @param {String} amount Amount to be formatted
 * @param {String} locale 
 * @param {String} currency 
 * @return Returns a formatted Currency
*/
const currencyFormattor = (amount, locale='en-US', currency='USD') => {
    // const number = parseInt(amount, 10)
    // if (number || number === 0) {
    if(amount){
        // return number.toLocaleString(locale, { style: 'currency', currency });
        // we need to show the dollar amount in Petco's Locale and Petco's currency instead of user Locale or user currency
        var formatter = new Intl.NumberFormat(locale, {
            style: 'currency',
            currency: currency,
          });
        return formatter.format(amount);
    }
    return '-'
}

export default currencyFormattor;


