

/**
 * this function returns formatted date with respective locale
 * @param {Date} date Date to be formatted
 * @param {String} locale 
 * @return Returns a formatted date
*/
export function dateFormattor (date, locale) {
    return date.toLocaleDateString(locale) === 'Invalid Date' ? '-' : date.toLocaleDateString(locale);
}