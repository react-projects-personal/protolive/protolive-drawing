import { applyMiddleware, createStore, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import promise from 'redux-promise';
import rootReducer from '../modules/rootReducer'
/*
  Configure Store - Development
*/
export default function configureStore(initialState) {

  const middlewares = [thunkMiddleware, promise]
  const middlewareEnhancer = applyMiddleware(...middlewares)

  // #region createStore : enhancer
  const enhancers = [middlewareEnhancer]
  // #endregion

  const composedEnhancers = compose(...enhancers)
  const store = createStore(rootReducer, initialState, composedEnhancers);

  return { store }

}
