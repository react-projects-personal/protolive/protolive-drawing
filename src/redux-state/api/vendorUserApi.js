
// import { API } from 'aws-amplify';
class VendorUser {

  
/**
   * this function makes API call create vendor user
   * @return Returns a promise
   */
  getAllVendorUsers = () => {
    return new Promise((resolve) => {

      const response = {
        "isBase64Encoded": false,
        "statusCode": 200,
        "body": [
          {
            "vendor_id": 1234,
            "user_id": "#1",
            "first_name": "vijay",
            "last_name": "chavre",
            "email": "abc@xyz.com",
            "status": "Active",
            "creation_date": "2019-06-20T09:17:01.000Z",
            "last_login": "2019-06-20T09:17:01.000Z"
          },
          {
            "vendor_id": 1564,
            "user_id": "#2",
            "first_name": "umesh",
            "last_name": "pawar",
            "email": "abc@xyz.com",
            "status": "Inactive",
            "update_date": "2019-06-25T07:51:06.000Z",
            "last_login": "2019-06-20T09:17:01.000Z"
          },
        ]
      }

      setTimeout(() => {
        resolve(response)
      }, 1000);
    })
  };
}

export default new VendorUser();
