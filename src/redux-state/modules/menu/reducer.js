

import menuActions from './actions';

const initialState = {
    collapsed: false
}

export default function menuReducer(state = initialState, action) {
    switch (action.type) {
        case menuActions.FETCH_MENU_COLLAPSE:
            //console.log('action is =', action.payload);
            return Object.assign({}, state, {
                collapsed: action.payload.collapsed
            });
        default:
            return state;
    }

}