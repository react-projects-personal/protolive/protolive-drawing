const menuActions = {

    /* Menu Actions */
    FETCH_MENU_COLLAPSE : 'fetch_menu_collapse',
    
    setCollapse: (collapsed) => ({
      type: menuActions.FETCH_MENU_COLLAPSE,
      payload: {
        collapsed
      }
    }),


  //   /**
  //  * This action creator function helps dispatching the createVendorUser() action.
  // */
  // createVendorUser: (payload) => {
  //   return async (dispatch) => {
  //     dispatch(vendorUserActions.createUser())
  //     try {
  //       const response = await VendorUserApi.createVendorUser(payload)
  //       dispatch(vendorUserActions.createUserSuccess(response))
  //     } catch (error) {
  //       dispatch(vendorUserActions.createUserFail(error))
  //     }
  //   }
  // }

    
  };
  
  export default menuActions
  