import React, { Component } from 'react'
import { Card, Form, Icon, Input, Button, Spin, Alert, Typography, Result } from "antd";
import { requestResetPassword} from 'services/aws_services';
import { loadData, removeData } from 'services/localstorage';
import Loading from 'components/Loading/Loading';
import image from 'assets/login_header.png';
import './Login.less';
import './Login.css';

const { Text } = Typography;

const spinIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

const forgotPasswordTitle =
  (<span style={{ fontFamily: 'Montserrat', fontSize: '22px', color: '#263238' }}>
    Forgot your password ?{' '}
  </span>);
class ForgotPassword extends Component {

  state = {
    showNewpasswordConfirm: false,
    isRedirectToLogin: false,
    forgotPasswordSubmit: false,
    newPasswordSubmit: false,
    err: '',
    infoMessage: 'An email was sent to your registered email address with a link to reset your password. Please check your email.'
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        try {
            this.setState({ forgotPasswordSubmit: true, err: '' });
            await requestResetPassword(values.username);
            this.setState({ showNewpasswordConfirm: true, forgotPasswordSubmit: false });
        } catch (err) {
          this.setState({
            err: (err.code === "UserNotFoundException")? 'Invalid email address. Please make sure that you enter the registered email address.' : 'Unable to process request at this time. Please try again in a few moments.',
            showNewpasswordConfirm: false,
            forgotPasswordSubmit: false,
            newPasswordSubmit: false
          });
        }

      }
    });
  }

  render() {
    if (this.state.isChecking) {
      return <Loading />;
    }
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="container">
        <div className="login">
          <Card
            cover={
              <img
                alt="example"
                src={image}              
              />
            }
            bordered={false}
            bodyStyle={{ padding: '0' }}
          >
            <Card bordered={false} title={forgotPasswordTitle} style={{ textAlign: 'center' }} bodyStyle={{ width: '100%' }}>
              <Spin indicator={spinIcon} tip="Logging In" spinning={this.state.newPasswordSubmit}>
                {
                  (this.state.showNewpasswordConfirm) ?
                    <Result
                      title="Request received"
                      subTitle={this.state.infoMessage}
                    />
                    : undefined
                }
                <Form onSubmit={this.handleSubmit} className="login-form" colon={false} hideRequiredMark={true}>
                { !this.state.showNewpasswordConfirm && 
                  <div>
                    <Form.Item label="Enter your email">
                        {getFieldDecorator('username', {
                          rules: [{ required: true, message: 'Please enter your username' }],
                        })(
                          <Input
                            size="large"
                            disabled={this.state.showNewpasswordConfirm}
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Username"
                          />,
                        )}
                    </Form.Item> 
                    <Form.Item>
                      <Button type="primary" size="large" htmlType="submit" className="login-form-button" loading={this.state.forgotPasswordSubmit}
                        style={{ borderRadius: '0' }}>
                        Submit
                      </Button>
                    </Form.Item>                    
                  </div>
                }
                </Form>
                {
                  (!!this.state.err) ?
                    <Alert message={<Text>{this.state.err}</Text>} type="error" showIcon />
                    : undefined
                }

              </Spin>
            </Card>
          </Card>
        </div>
      </div>
    )
  }
}

const WrappedForgotPasswordForm = Form.create({ name: 'forgot_password' })(ForgotPassword);

export default WrappedForgotPasswordForm;