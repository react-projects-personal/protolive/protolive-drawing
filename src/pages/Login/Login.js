import React, { Component } from 'react'
import { Card, Form, Icon, Input, Button, Spin, Alert, Typography } from "antd";
import { Redirect, Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { saveData, loadData, removeData } from 'services/localstorage';
import Loading from 'components/Loading/Loading';
import image from 'assets/login_header.png';
import './Login.less';
import './Login.css';

// import authConfig from 'services/authConfig.prod.json';
const { Text } = Typography;
const spinIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

const loginTitle =
  (<span style={{ fontFamily: 'Montserrat', fontSize: '22px', color: '#263238' }}>
    Logon to {' '}
    <span style={{ color: '#29ABE2' }}>
      Drawing{' '}
    </span>
    <span style={{ fontFamily: 'Montserrat' }}>
      <strong style={{ color: 'red' }}>
        App
            </strong>
      <span style={{ color: '#546E7A', fontWeight: '300' }}>

      </span>
    </span>
  </span>);
class Login extends Component {
  _isMounted = false;
  state = {
    showChallenge: false,
    user: '',
    loggedIn: false,
    loginSubmit: false,
    challengeSubmit: false,
    isChecking: true,
    err: '',
    SAMLerr: ''
  };



  async componentDidMount() {
    this._isMounted = true;
    this.setState({ isChecking: false, loggedIn: false });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleSubmit = (e) => {
    e.preventDefault();


    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        this.setState({ loginSubmit: true, isChecking: true, });
        setTimeout(() => {
          this.setState({ loginSubmit: false, loggedIn: true, isChecking: false });
        }, 1000)
      }
    });

  }

  handleReset = () => {
    this.setState({ showChallenge: false, user: '', err: '', loginSubmit: false, challengeSubmit: false });
    this.props.form.resetFields();
  };

  render() {

    if (this.state.isChecking && !this.state.loggedIn) {
      return <Loading />;
    }

    if (this.state.loggedIn) {
      return (
        <Redirect to="/app/home" />
      );
    }

    const { getFieldDecorator } = this.props.form;
    return (
      <div className="container">
        <div className="login">
          <Card
            // cover={
            //   <img
            //     alt="example"
            //     src={image}
            //   />
            // }
            bordered={false}
            bodyStyle={{ padding: '0' }}
          >
            <Card bordered={false} title={loginTitle} style={{ textAlign: 'center' }} bodyStyle={{ width: '100%' }}>
              <Spin indicator={spinIcon} tip="Logging In" spinning={this.state.loginSubmit}>
                <Form onSubmit={this.handleSubmit} className="login-form" colon={false} hideRequiredMark={true}>
                  <Form.Item>
                    {getFieldDecorator('username', {
                      rules: [{ required: true, message: 'Please enter your username' }],
                    })(
                      <Input
                        size="large"
                        disabled={this.state.showChallenge}
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Username"
                      />,
                    )}
                  </Form.Item>
                  <Form.Item>
                    {getFieldDecorator('password', {
                      rules: [{ required: true, message: 'Please enter your password' }],
                    })(
                      <Input
                        size="large"
                        disabled={this.state.showChallenge}
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Password"
                      />,
                    )}
                  </Form.Item>
                  <Form.Item>

                    <Button type="primary" size="large" htmlType="submit" className="login-form-button" loading={this.state.loginSubmit}
                    // style={{borderRadius: '0'}}
                    >
                      Log in
                  </Button>
                    {/* <Link type="link" to="/forgotPassword"className="login-form-forgot-left">Forgot Password?</Link> */}
                    {
                      (this.state.showChallenge) ?
                        <Button type="link" className="login-form-try" onClick={this.handleReset} icon="reload">Try Again</Button>
                        : undefined
                    }
                  </Form.Item>
                </Form>
                <Alert message={<Text>{'Temporarily use any username or passoword to get inside the app'}</Text>} type="info" showIcon />
                {/* <Button type="link"  onClick={async() => {
                  saveData('ssoclient', true);                  
                  configureAuth(true);
                  const { ssoFragments }= authConfig;
                  const uri = `${ssoFragments.domain}${ssoFragments.identity_provider}${ssoFragments.redirect_uri}${ssoFragments.response_type}${ssoFragments.client_id}${ssoFragments.scope}`;                  
                  window.location.href= uri;                  
              }}>
                Petco Partner Login
              </Button> */}
                {
                  (!!this.state.err) ?
                    <Alert message={<Text>{this.state.err}</Text>} type="error" showIcon />
                    : undefined
                }
              </Spin>
            </Card>
          </Card>
        </div>
      </div>
    )
  }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);

export default withRouter(WrappedNormalLoginForm);
