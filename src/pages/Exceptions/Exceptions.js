import React, { Component } from 'react';
import {Row, Col} from 'antd';
// import { withRouter } from "react-router";
import config from './typeConfigs';
import img404 from 'assets/404.svg';

export default class Exceptions extends Component {
    state = {
        code: this.props.location.state? this.props.location.state.code: 404
    }

  render() {
    
    const {title, desc, message } = config[this.state.code];

    return (
        <div>
            <Row type="flex" justify="center">
                <Col span={4}><div style={{height: '50px'}}/></Col>
            </Row>
            <Row type="flex" justify="center">
                <Col span={4}>
                    <img src={img404} alt={this.state.code} />
                    {/* {img} */}
                </Col>
                <Col span={6} style={{color: '#434e59'}}>
                    <h1>{title}</h1>
                    <h2>{desc}</h2>
                    <p>{message}</p>
                </Col>
            </Row>
        </div>
    )
  }
}
