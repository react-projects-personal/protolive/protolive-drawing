// import img404 from 'assets/404.svg';

const exceptions = {
  403: {
    // img: <img src={img404} alt='404' />,
    title: '403',
    desc: 'No Permission',
    message: 'You do not have permission to access this page',
  },
  404: {
    // img: <img src={img404} alt='404' />,
    title: '404',
    desc: 'Page Not Found',
    message: 'The page you are looking for does not exist'
  },
  512: {
    // img: <img src={img404} alt='404' />,
    title: '512',
    desc: 'Invitation Expired',
    message: 'The invitation you are trying to access has expired'
  },
  513: {
    // img: <img src={img404} alt='404' />,
    title: '513',
    desc: 'Reset Password Link Expired',
    message: 'Please initiate forgot password request again'
  }
};

export default exceptions;
