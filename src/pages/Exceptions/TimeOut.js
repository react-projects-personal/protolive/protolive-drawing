import React from 'react';
import { Card, Result } from "antd";
import { Link } from "react-router-dom";
import './Timeout.less';

export default function TimeOut() {
    return (
        <div className="container-to"> 
            <div className="login">
                <Card bordered={false} style={{textAlign: 'center'}} bodyStyle={{width: '100%'}}>
                    <Result
                        status="warning"
                        title="You have been logged out due to inactivity."
                        extra={
                        <Link to="/">                            
                            Back to Login                            
                        </Link>    
                        }/>
                </Card>
            </div>            
        </div>
    )
}
